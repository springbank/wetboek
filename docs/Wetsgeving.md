---
name: Home
---

# Algemene Plaatselijke Verordening Vlieland

Hieronder op deze pagina kun je onze algemene wetsgeving zien voor onze stad Vlieland. De algemene wetten van onze stad, dat zie je in een andere pagina! Hou er rekening mee, dat onze hogere Staffteam leden je ten alle tijden een andere sanctie kunnen opzetten. 

### Artikel 1 - FailRP & Powergaming

1. Hij/zij die opzettelijk een roleplay van zeer slechte kwaliteit uitspeelt wordt gestraft volgens categorie 1. 
2. De overtreding die hij/zij aangaat op dat moment, wordt gezien als een 'FailRP'. 
3. Hij/zij die opzettelijk de roleplay zodanig vormt dat er een oneerlijke of onrealistische draai aan het verhaal geeft, of de roleplay van de andere partij zelf invult door een onrealistische dwang, wordt gestraft volgens categorie 1.
4. De overtreding die hij/zij aangaat op dat moment, wordt dan gezien als 'Powergaming'

- Enkele voorbeelden van FailRP & Powergaming zijn:
* Niet meewerken aan een roleplayscenario of een /me-command, bijvoorbeeld opzettelijk een onjuiste portofrequentie doorgeven nadat er een roleplayactie is uitgevoerd om dit te achterhalen;
* Tijdens een achtervolging je voertuig in de garage zetten om te voorkomen dat je je voertuig kwijtraakt;
* Geen roleplay aangaan na of je niets aantrekken van een (ernstige) verwonding of (verkeers)ongeval;
* HeadID's misbruiken om spelers te vinden/identificeren tijdens een roleplayscenario;
* Onjuist of onrealistisch gebruik maken van het F6-menu;
* Overtredingen van de overval regels kunnen volgens dit artikel worden bestraft;
* In het water zwemmen voor een onrealistisch lange tijd;
* In een portofoon praten terwijl je aan het zwemmen bent of onderwater bent;
* Stelen van een voertuig van een burger zonder enige geldige reden;

### Artikel 2 - Beroepskleding

1. Er wordt geacht dat de kleding wordt gedragen die is verstrekt door de baas voor het uitvoeren van het beroep. Wordt dit niet gedaan, zal er gestraft worden met een straf volgens categorie 1 of zelfs 2 bij meerdere overtredingen.
2. Hij/zij die als niet ambtenaar kleding draagt van één van de overheids banen zal gestraft worden met een straf volgens categorie 1 of zelfs 2 bij meerdere overtredingen. 
3. Kleding die wordt gegeven aan officiele groeperingen/families, is ten strengste verboden om te dragen als geen lid van die groepering/familie.
4. Overtreding van lid 3 zal worden bestraft met een straf uit van de 1e categorie.
5. Onder ambtenaar vallen de mensen in dienst bij één van de volgende instanties:
    * Politie
    * Justitie
    * Ambulance
    * Wegenwacht

### Artikel 3 - Value of Life

1. Hij/zij die geen waarde hecht aan het leven van zijn karakter zal gestraft worden met een straf volgens de 2e categorie.
2. De overtreding zoals beschreven in lid 1 staat bekend als “no value of life”.
3. Enkele voorbeelden van “no value of life”:
    * zodra iemand een pistool op je richt binnen 50 meter en er geen beschutting is, blijf je staan, werk je mee en val je deze persoon niet aan.
    * Zodra iemand een steek- of slagwapen binnen armlengte afstand van je trekt dan werk je mee, doe je dit niet ben je niet zuinig met je leven aangezien je dat bewust het risico neemt neergestoken of neergeslagen te worden.
    * Met een helikopter vlak boven de grond zweven om goederen en/of personen te transporteren.
    * Elke andere vorm waarmee je bewust je eigen leven in een té groot gevaar brengt valt onder no value of life, dus gebruik je gezonde verstand om de situatie in te schatten.
    * Zodra een persoon een wapen in zijn hand heeft en jij gaat hem proberen te boeien of richting hem te rennen word dit gezien als no value of life. Wanneer jij van dat persoon weet dat hij/zij een vuur-, steek- of slagwapen op zak heeft, zal je ervoor moeten zorgen hem/haar te overmeesteren door enige vorm van bedreigende middelen toe te passen.
    * Het valt onder no value of life om de noodknop in te drukken terwijl iemand een vuurwapen, steekwapen of slagwapen op je richt.
    * Het is geen no value of life indien een persoon in een voertuig met deuren zit niet meewerkt, als je deze van de buitenkant bedreigt met een steek- of slagwapen.
    * Ook valt het niet onder no value of life om een vuurwapen te trekken als je bedreigd wordt met een steek- of slagwapen.
    * De beslissing van een stafflid betreft alle bovenstaande en overige situaties is altijd leidend.

### Artikel 4 - Exploits

1. Hij/zij die moedwillig een bug en/of exploit misbruikt om voor zichzelf of anderen profijt te behalen, ongeacht in welke middelen, zal per direct gestraft worden volgens categorie 5.
2. Wanneer het om bedragen gaat hoger dan €50.000,- zal de straf genoemd in lid 1 verhoogd worden naar de 6e categorie.
3. Wanneer het om bedragen gaat hoger dan €100.000,- zal de straf genoemd in lid 1 en lid 2 verhoogd worden naar de 7e categorie.
4. De bedragen zoals genoemd in lid 2 en lid 3 zijn ook van toepassing voor voorwerpen, hiervoor zal dan de waarde van deze voorwerpen worden getaxeerd. Taxering geschiedt hier op basis van catalogus of -marktwaarde.
5. De bedragen en/of materialen zullen per direct teruggegeven worden aan een stafflid. Het stafflid dient deze per direct te vernietigen. Hiernaast zullen de betrokken accounts ook volledig verwijderd worden (categorie 8).
6. Naast de in lid 1, 2 en 3 genoemde straffen zal de overtreder ook 25% van de waarde extra terug moeten betalen.