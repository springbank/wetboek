---
name: Home
---

# Sancties & Discord servers VlielandRP

Dit is de verzamelpagina rondom de Wet- en Regelgeving van Vlieland.
Voordat je als nieuwe burger in de stad komt, is het van belang op de hoogte te zijn van de huidige regels. 

_Deze regels zijn opgesteld door het bestuur van de stad. Alle wijzigingen die doorgevoerd worden, zullen per direct in gang gezet worden. Blijf daarom goed op de hoogte!_ 

### Vlieland - Strafbepaling

| Categorie | Beschrijving |  | 
|---|---|:---:|
|Categorie 1| Waarschuwing + 25 taakstraffen |
|Categorie 2| Ban van maximaal 1 dag |
|Categorie 3| Ban van maximaal 3 dagen |
|Categorie 4| Ban van maximaal 1 week |
|Categorie 5| Ban van maximaal 2 weken |
|Categorie 6| Ban van maximaal 1 maand |
|Categorie 7| Pemanente ban voor alle gebruikers op je netwerk|
|Categorie 8| Volledige account reset (wipe) |

## Discord Servers

Vlieland heeft diverse Discord Servers die van belang kunnen zijn buiten de stad om:

| Server | Beschrijving | Invite link |
|---|---|:---:|
|Vlieland RolePlay| De algemene discord server van Vlieland | [Invite](https://discord.gg/vlielandrp) |
|Vlieland Hulpdiensten| De hulpdiensten discord server van Vlieland | [Invite](https://discord.gg/WYMv93wznk) |
